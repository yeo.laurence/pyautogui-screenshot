A simple way to video capture your laptop or monitor screen while you play computer games.

My secondary goal is to learn how to apply this work to my VX Chart trading of the e-minis S&P. It is to capture changes in the v1k SLA colours (red to blue and vice versa) and then to output an audio alert. I think even the v504 SLA change of colour can be monitored concurrently too.
