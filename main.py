import numpy as np
import cv2 as cv
import os
import pyautogui

while(True):
    screenshot = pyautogui.screenshot()
    screenshot = np.array(screenshot)
    # Convert RGB to BGR
    screenshot = screenshot[:, :, ::-1].copy()
    # Display the resulting frame
    cv.imshow('Computer Vision', screenshot)
    if cv.waitKey(1) == ord('q'):
        cv.destroyAllWindows()
        break
# When everything done, release the capture

print('Done!')